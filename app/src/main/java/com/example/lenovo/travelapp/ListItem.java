package com.example.lenovo.travelapp;

import android.content.Context;
import android.content.Intent;

public class ListItem {

    private String item_title;
    private String arrival;
    private String departure;
    private String arrival_time;
    private String departure_time;
    private String travel_time;
    private String item_id;
    private String price;
    private String link_type;
    private int imageResource;

    public ListItem(String item_id, String item_title, String arrival, String departure, String arrival_time, String departure_time, String price, String travel_time, String link_type, int imageResource) {
        this.item_id = item_id;
        this.arrival = arrival;
        this.departure = departure;
        this.arrival_time = arrival_time;
        this.departure_time = departure_time;
        this.price = price;
        this.travel_time = travel_time;
        this.item_title = item_title;
        this.link_type = link_type;
        this.imageResource = imageResource;
    }


    public void seeMap(String departure, String arrival, Context context) {
        Intent intent = new Intent(context.getApplicationContext(), MapsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("departure", departure);
        intent.putExtra("arrival", arrival);
        context.startActivity(intent);
    }

    public String getItem_title() {
        return item_title;
    }

    public void setItem_title(String item_title) {
        this.item_title = item_title;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival_time() {
        return arrival_time;
    }

    public void setArrival_time(String arrival_time) {
        this.arrival_time = arrival_time;
    }

    public String getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(String departure_time) {
        this.departure_time = departure_time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTravel_time() {
        return travel_time;
    }

    public void setTravel_time(String travel_time) {
        this.travel_time = travel_time;
    }


    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getLink_type() {
        return link_type;
    }

    public void setLink_type(String link_type) {
        this.link_type = link_type;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }
}