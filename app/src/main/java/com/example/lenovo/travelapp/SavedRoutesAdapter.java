package com.example.lenovo.travelapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SavedRoutesAdapter extends RecyclerView.Adapter<SavedRoutesAdapter.RecyclerViewHolder> {
    private ArrayList<ListItem> list_Items;
    private OnItemClickListener itemClickListener;


    public interface OnItemClickListener {
        void onDeleteClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }


    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_id, tv_arrival, tv_departure, tv_arrival_time, tv_departure_time, tv_price, tv_travel_time, tv_item_title;
        ImageView imageView;

        public RecyclerViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            tv_item_id = itemView.findViewById(R.id.tv_item_id);
            tv_arrival = itemView.findViewById(R.id.item_arrival);
            tv_departure = itemView.findViewById(R.id.item_departure);
            tv_arrival_time = itemView.findViewById(R.id.item_arrival_time);
            tv_departure_time = itemView.findViewById(R.id.item_departure_time);
            tv_price = itemView.findViewById(R.id.item_price);
            tv_travel_time = itemView.findViewById(R.id.item_travel_time);
            tv_item_title = itemView.findViewById(R.id.item_title);
            imageView = itemView.findViewById(R.id.item_imageView);


            imageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION)
                            listener.onDeleteClick(position);
                    }
                }
            });

        }
    }


    public SavedRoutesAdapter(ArrayList<ListItem> listItems) {
        list_Items = listItems;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.train_list_item, parent, false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(v, itemClickListener);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder recyclerViewHolder, int i) {
        ListItem currentItem = list_Items.get(i);
        recyclerViewHolder.tv_item_id.setText(currentItem.getItem_id());
        recyclerViewHolder.imageView.setImageResource(currentItem.getImageResource());
        recyclerViewHolder.tv_arrival.setText(currentItem.getArrival());
        recyclerViewHolder.tv_departure.setText(currentItem.getDeparture());
        recyclerViewHolder.tv_arrival_time.setText(currentItem.getArrival_time());
        recyclerViewHolder.tv_departure_time.setText(currentItem.getDeparture_time());
        recyclerViewHolder.tv_price.setText(String.valueOf(currentItem.getPrice()));
        recyclerViewHolder.tv_travel_time.setText(currentItem.getTravel_time());
        recyclerViewHolder.tv_item_title.setText(currentItem.getItem_title());
    }

    @Override
    public int getItemCount() {
        return list_Items.size();
    }
}
