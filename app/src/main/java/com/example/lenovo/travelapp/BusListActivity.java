package com.example.lenovo.travelapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class BusListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private String departure;
    private String arrival;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_list);
        setDrawer();
        setMenu();
        getIntentData();
        loadBuses();
    }


    public void setDrawer() {
        DrawerLayout drawerLayout = findViewById(R.id.bus_list_drawerLayout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    public void getIntentData() {
        arrival = getIntent().getExtras().getString("arrival");
        departure = getIntent().getExtras().getString("departure");
        String date = getIntent().getExtras().getString("date");
    }


    public void setMenu() {
        NavigationView navigation = findViewById(R.id.list_navigationView);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.app_search:
                        goHome();
                        return true;

                    case R.id.app_logout:
                        logoutUser();
                        return true;

                    case R.id.app_user_profile:
                        goProfile();
                        return true;

                    case R.id.app_map:

                        goMaps();
                        return true;
                }
                return false;
            }
        });
    }

    private void logoutUser() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signOut();
        if (auth.getCurrentUser() == null) {
            startActivity(new Intent(BusListActivity.this, LoginActivity.class));
            finish();
        }
    }

    public void loadBuses() {

        new webScrap().execute();

    }

    public void setRecyclerviewDecoration() {
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
    }


    public class webScrap extends AsyncTask<Void, Void, Void> {
        ArrayList<ListItem> buses = new ArrayList<>();
        String details;
        String departureText = departure;
        String arrivalText = arrival;


        @Override
        protected Void doInBackground(Void... voids) {
            try {
                int i = 1;
                departureText = formatStringsForURL(departureText);
                arrivalText = formatStringsForURL(arrivalText);
                Document document = Jsoup.connect("https://www.autogari.ro/Transport/" + departureText + "-" + arrivalText).get();

                Elements bodies = document.select("tbody");
                for (Element body : bodies) {
                    Elements rows = body.select("tr.cursaRow.cursa_ok");
                    for (Element row : rows) {
                        Bus bus = new Bus();
                        if (row != null) {
                            Elements element=new Elements();
                            String capWords=capitalizeWord(departure);
                            bus.setDeparture(capWords);
                            capWords=capitalizeWord(arrival);
                            bus.setArrival(capWords);

                            tryVehicletype("td.normal.c-auto span.mijloctr20", row, bus);
                            tryVehicletype("td.normal.c-auto span.mijloctr2", row, bus);
                            tryVehicletype("td.normal.c-auto span.mijloctr10", row, bus);
                            tryVehicletype("td.normal.c-auto span.mijloctr21", row, bus);

                            details = row.select("td.normal.c-plecare div.bad").text();
                            bus.setDeparture_time(details);
                            details = row.select("td.normal.c-durata").text();
                            bus.setTravel_time(details);
                            details = row.select("td.normal.c-sosire div.bad").text();
                            bus.setArrival_time(details);
                            element = row.select("td.normal.c-sigla div img");
                            details = element.attr("title");
                            if(!details.equals(""))
                                bus.setItem_title(details);
                            element = row.select("td.normal.c-sigla div");
                            details = element.text();
                            Log.d("massaaa", details);
                            if(!details.equals(""))
                                bus.setItem_title(details);
                            details = row.select("td.normal.c-pret").text();
                            bus.setPrice(details);
                            bus.setNumber("Varianta#" + i++);


                        }
                        addBus(bus, buses);
                    }
                }


            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            buildRecyclerView(buses);
        }
    }

    public void tryVehicletype(String s, Element row, Bus bus)
    {
        Elements element;
        String details="";
        element = row.select(s);
        details = element.attr("title");
        if(!details.equals(""))
            bus.setItem_id(details);
    }


    public void buildRecyclerView(final ArrayList<ListItem> buses) {
        recyclerView = findViewById(R.id.bus_recyclerView);
        layoutManager = new LinearLayoutManager(this);
        setRecyclerviewDecoration();

        BusRecyclerViewAdapter busRecyclerViewAdapter = new BusRecyclerViewAdapter(buses);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(busRecyclerViewAdapter);

        busRecyclerViewAdapter.setOnItemClickListener(new BusRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onMapClick(int position) {
                buses.get(position).seeMap(buses.get(position).getDeparture(), buses.get(position).getArrival(), getApplicationContext());
            }
        });
    }


    public void addBus(Bus bus, ArrayList<ListItem> buses) {
        buses.add(new ListItem(bus.getItem_title(), bus.getItem_id(), bus.getArrival(), bus.getDeparture(), bus.getArrival_time(), bus.getDeparture_time(), "Preț: " + bus.getPrice(), "Timpul călătoriei: " + bus.getTravel_time(), bus.getNumber(), R.drawable.map_icon));
    }


    public String formatStringsForURL(String string) {
        return string.replaceAll(" ", "");
    }


    private void goHome() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
        finish();
    }

    private void goMaps() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
        finish();
    }

    private void goProfile() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
        finish();
    }

    public String capitalizeWord(String input)
    {
        //String output = input.substring(0, 1).toUpperCase() + input.substring(1);
        String words[]=input.split(" ");
        String capitalizeWord="";
        for(String w:words){
            String first=w.substring(0,1);
            String afterfirst=w.substring(1);
            capitalizeWord+=first.toUpperCase()+afterfirst+" ";
        }
        return capitalizeWord.trim();
    }

}