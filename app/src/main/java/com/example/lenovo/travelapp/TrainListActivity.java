package com.example.lenovo.travelapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;

public class TrainListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private String date, departure, arrival, options;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_list);
        auth = FirebaseAuth.getInstance();

        setDrawer();
        setMenu();
        getIntentData();
        loadTrains();
    }


    public void setDrawer() {
        DrawerLayout drawerLayout = findViewById(R.id.train_list_drawerLayout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    public void getIntentData() {
        arrival = getIntent().getExtras().getString("arrival");
        departure = getIntent().getExtras().getString("departure");
        date = getIntent().getExtras().getString("date");
        options = getIntent().getExtras().getString("options");
    }


    public void setMenu() {
        NavigationView navigation = findViewById(R.id.list_navigationView);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.app_search:
                        goHome();
                        return true;

                    case R.id.app_logout:
                        logoutUser();
                        return true;

                    case R.id.app_user_profile:
                        goProfile();
                        return true;

                    case R.id.app_map:

                        goMaps();
                        return true;
                }
                return false;
            }
        });
    }



    private void logoutUser() {
        auth.signOut();
        if (auth.getCurrentUser() == null) {
            startActivity(new Intent(TrainListActivity.this, LoginActivity.class));
            finish();
        }
    }

    public void loadTrains() {
        new webScrap().execute();
    }

    public class webScrap extends AsyncTask<Void, Void, Void> {
        ArrayList<ListItem> trains = new ArrayList<>();

        String departureText = departure;
        String arrivalText = arrival;


        @Override
        protected Void doInBackground(Void... voids) {
            departureText = formatStringsForURL(departureText);
            arrivalText = formatStringsForURL(arrivalText);
            String urls[] = new String[]{
                    "https://mytrain.ro/ro/legaturi/" + departureText + "/" + arrivalText + "/" + date + "/#r",
                    "https://mytrain.ro/ro/legaturi/" + departureText + "-h" + "/" + arrivalText + "/" + date + "/#r",
                    "https://mytrain.ro/ro/legaturi/" + departureText + "/" + arrivalText + "-h" + "/" + date + "/#r",
                    "https://mytrain.ro/ro/legaturi/" + departureText + "-h" + "/" + arrivalText + "-h" + "/" + date + "/#r"

            };
            for (int i = 0; i < urls.length; i++) {

                Document document;
                try {
                    document = Jsoup.connect(urls[i]).get();
                    scrapData(document, trains);
                    break;
                } catch (IOException e) {
                    if (i == urls.length - 1)
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getApplicationContext(), "There are no trains for this route", Toast.LENGTH_SHORT).show();
                            }
                        });
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            buildRecyclerView(trains);
        }
    }

    public void scrapData(Document document, ArrayList<ListItem> trains) {
        String details = "";
        for (Element row : document.select("div.route")) {
            Train train = new Train();
            if (!row.select("div.col-md-3").text().equals("")) {
                details = row.select("div.col-md-3 p.details").text();
                String[] splittedSpan = details.split(", ");
                train.setTravel_time(splittedSpan[0]);
                train.setLink_type(splittedSpan[1]);
                String[] priceSpan = splittedSpan[2].split(" ");
                double price = Double.valueOf(priceSpan[0]);
                double priceType = trainPrice(price);
                train.setPrice(priceType + " " + priceSpan[1]);
                train.setItem_id(row.select("div.col-md-3 p.badge").text());
            }

            if (!row.select("div.col-md-9 p.summary span").text().equals("")) {
                getSpanText(train, row);
            }

            if (train.getLink_type().equals("legatura directa")) {
                addTrain(train, trains);
            } else {
                String[] legatura = train.getLink_type().split(" ");
                for (int k = 0; k <= Integer.valueOf(legatura[0]); k++) {
                    Element element = row.select("p.summary").get(k);
                    getSpanText(train, element);
                    element = row.select("div.train.hidden-xs").get(k);
                    String travel_details = element.select("p.travel").text();
                    String result = travel_details.substring(travel_details.indexOf(" ") + 1, travel_details.indexOf("p"));

                    train.setTravel_time(result);
                    String[] splittedText = travel_details.split(" ");
                    double price = Double.valueOf(splittedText[splittedText.length - 2]);
                    double priceType = trainPrice(price);
                    train.setPrice(priceType + " " + splittedText[splittedText.length - 1]);
                    if (k >= 1) {
                        train.setItem_id("Schimbul " + k);
                    }
                    addTrain(train, trains);
                }

            }
        }
    }

    public double trainPrice(double price) {
        double returnPrice = 0.0;
        switch (options) {
            case "Adult":
                returnPrice = price;
                break;
            case "School Student":
                returnPrice = price / 2.0;
                break;
            case "University Student":
                returnPrice = 0.0;
                break;
            case "Pensioner":
                returnPrice = price / 2.0;
                break;
        }
        return returnPrice;

    }

    public void buildRecyclerView(final ArrayList<ListItem> trains) {
        recyclerView = findViewById(R.id.train_recyclerView);
        layoutManager = new LinearLayoutManager(this);
        TrainRecyclerViewAdapter trainRecyclerViewAdapter = new TrainRecyclerViewAdapter(trains);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(trainRecyclerViewAdapter);

        trainRecyclerViewAdapter.setOnItemClickListener(new TrainRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onMapClick(int position) {
                trains.get(position).seeMap(trains.get(position).getDeparture(), trains.get(position).getArrival(), getApplicationContext());
            }
        });
    }


    public void getSpanText(Train train, Element element) {
        String span = "";
        String departure = "";
        String arrival = "";
        span = element.select("span").get(0).text();
        span = formatSpan(span);
        train.setItem_title(span);
        span = element.select("span").get(1).text();
        span = formatSpan(span);
        String[] splittedSpan = span.split(" ");
        for (int i = 0; i < splittedSpan.length - 1; i++)
            departure += splittedSpan[i] + " ";
        train.setDeparture(departure);
        train.setDeparture_time(splittedSpan[splittedSpan.length - 1]);
        span = element.select("span").get(2).text();
        span = formatSpan(span);
        splittedSpan = span.split(" ");
        for (int i = 0; i < splittedSpan.length - 1; i++)
            arrival += splittedSpan[i] + " ";
        train.setArrival(arrival);
        train.setArrival_time(splittedSpan[splittedSpan.length - 1]);
    }

    public void addTrain(Train train, ArrayList<ListItem> trains) {
        trains.add(new ListItem(train.getItem_id(), train.getItem_title(), train.getArrival(), train.getDeparture(), train.getArrival_time(), train.getDeparture_time(), "Preț: " + train.getPrice(), "Timpul călătoriei: " + train.getTravel_time(), train.getLink_type(), R.drawable.map_icon));
    }

    public String formatSpan(String span) {
        int spaceIndex = span.indexOf(" (");
        if (spaceIndex != -1) {
            span = span.substring(0, spaceIndex);
        }
        return span;
    }

    public String formatStringsForURL(String string) {
        String copy = string.replaceAll(" ", "-").toLowerCase();
        return string.replaceAll(" ", "-").toLowerCase();
    }


    private void goHome() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
        finish();
    }

    private void goMaps() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
        finish();
    }
    private void goProfile() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
        finish();
    }
}