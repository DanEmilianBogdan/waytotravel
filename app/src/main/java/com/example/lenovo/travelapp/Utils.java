package com.example.lenovo.travelapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class Utils {

    public static String[] readFile(Context context, String[] locations, String file) {
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(context.getAssets().open(file), StandardCharsets.UTF_8);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            int linesNr = 0;
            while (bufferedReader.readLine() != null) linesNr++;
            bufferedReader.close();
             inputStreamReader = new InputStreamReader(context.getAssets().open(file), StandardCharsets.UTF_8);
             bufferedReader = new BufferedReader(inputStreamReader);
            locations = new String[linesNr];
            int i = 0;
            String lines;
            while ((lines = bufferedReader.readLine()) != null) {
                locations[i++] = lines;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return locations;
    }


    public static boolean verifyNetworkConnection(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;


        return connected;
    }

    public static void toastMessage(Context context, String message) {
        Toast toast = Toast.makeText(context.getApplicationContext(), message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, -140);
        toast.show();
    }
}