package com.example.lenovo.travelapp;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SearchActivity extends AppCompatActivity implements
        View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener,
        DatePickerDialog.OnDateSetListener {

    private Button btn_select_date, btn_select_options;
    private FusedLocationProviderClient fusedLocationClient;
    private AutoCompleteTextView actv_departure, actv_arrival;
    private String date = "", options = "Adult";
    protected Location mLastLocation;
    private TextView tv_location;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initialize();
        setMenu();
    }

    public void initialize() {
        // setDrawer();
        Button btn_search = findViewById(R.id.btn_search);
        btn_select_date = findViewById(R.id.btn_select_date);
        btn_select_options = findViewById(R.id.btn_options);
        actv_departure = findViewById(R.id.actv_departure);
        actv_arrival = findViewById(R.id.actv_arrival);
        tv_location = findViewById(R.id.location);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        setTrainAutocompleteAdapter(actv_arrival, actv_departure);

        btn_search.setOnClickListener(this);
        btn_select_date.setOnClickListener(this);

        getLastLocation();
        //  setKeyboardEnter();
        registerForContextMenu(btn_select_options);

    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }
        fusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();

                            reverseGeocoding(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                        } else {
                            Log.w("ErrorA", "getLastLocation:exception", task.getException());

                        }
                    }
                });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_search_options, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.search_by_bus:
                options = item.getTitle().toString();
                btn_select_options.setText(options);
                setBusAutocompleteAdapter(actv_arrival, actv_departure);
                return true;
            case R.id.search_by_both:
                options = item.getTitle().toString();
                btn_select_options.setText(options);
                return true;
            case R.id.search_adult:
                options = item.getTitle().toString();
                setTrainAutocompleteAdapter(actv_arrival, actv_departure);
                btn_select_options.setText("T/" + options);
                return true;
            case R.id.search_uStudent:
                options = item.getTitle().toString();
                setTrainAutocompleteAdapter(actv_arrival, actv_departure);
                btn_select_options.setText("T/" + options);
                return true;
            case R.id.search_sStudent:
                options = item.getTitle().toString();
                setTrainAutocompleteAdapter(actv_arrival, actv_departure);
                btn_select_options.setText("T/" + options);
                return true;
            case R.id.search_pensioner:
                options = item.getTitle().toString();
                setTrainAutocompleteAdapter(actv_arrival, actv_departure);
                btn_select_options.setText("T/" + options);
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }

    public void setDrawer() {
        DrawerLayout drawerLayout = findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    public void setMenu() {
        NavigationView navigation = findViewById(R.id.navigationView);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.app_search:
                        goHome();
                        return true;

                    case R.id.app_logout:
                        logoutUser();
                        return true;

                    case R.id.app_user_profile:
                        goProfile();
                        return true;

                    case R.id.app_map:
                        goMaps();
                        return true;
                }
                return false;
            }
        });
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_search) {
            if (Utils.verifyNetworkConnection(this)) {
                executeSearch();
            } else
                Toast.makeText(this, "You must connect to the internet", Toast.LENGTH_SHORT).show();
        } else if (view.getId() == R.id.btn_select_date) {
            DialogFragment datePicker = new DatePickerFragment();
            datePicker.show(getSupportFragmentManager(), "Date Picker");
        }

    }

    private void logoutUser() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signOut();
        if (auth.getCurrentUser() == null) {
            startActivity(new Intent(SearchActivity.this, LoginActivity.class));
            finish();
        }
    }

    public void searchTrain(String options) {
        Intent intent = new Intent(this, TrainListActivity.class);
        intent.putExtra("departure", actv_departure.getText().toString());
        intent.putExtra("arrival", actv_arrival.getText().toString());
        intent.putExtra("date", date);
        intent.putExtra("options", options);
        startActivity(intent);
    }

    public void searchBus() {
        Intent intent = new Intent(this, BusListActivity.class);
        intent.putExtra("departure", actv_departure.getText().toString());
        intent.putExtra("arrival", actv_arrival.getText().toString());
        intent.putExtra("date", date);
        startActivity(intent);
    }

    public void executeSearch() {
        switch (options) {
            case "Adult":
                searchTrain(options);
                break;
            case "School Student":
                searchTrain(options);
                break;
            case "University Student":
                searchTrain(options);
                break;
            case "Pensioner":
                searchTrain(options);
                break;
            case "Search by bus":
                searchBus();
                break;
            case "Search by both":
                //  Log.d("mamaa", options);
                break;

        }

    }

    private void collapseKeyboard(View view) {
         view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }






    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        month = month + 1;
        date = "0" + month + "-" + dayOfMonth;
        String currentDate = DateFormat.getDateInstance(DateFormat.SHORT).format(calendar.getTime());
        btn_select_date.setText(currentDate);

    }

    private void goHome() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
        finish();
    }

    private void goMaps() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
        finish();
    }

    private void goProfile() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void reverseGeocoding(double latitude, double longitude) {

        Geocoder geocoder = new Geocoder(SearchActivity.this);
        Address address;
        List<Address> list = new ArrayList<>();

        try {
            list = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            Log.d("AAA", "geoLocate: IOException: " + e.getMessage());
        }
        if (list.size() > 0) {
            address = list.get(0);
            Log.d("AAA", "geoCoordinates: found a location: " + address.toString());
            tv_location.setText(address.getLocality());

        } else {
            Utils.toastMessage(getApplicationContext(), "Couldn't find location");
        }

    }

    private void initAutoCompleteSearch(AutoCompleteTextView autoCompleteTextView1, AutoCompleteTextView autoCompleteTextView2) {
        PlaceAutocompleteAdapter placeAutocompleteAdapter;
        GoogleApiClient googleApiClient;
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build();
        final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(new LatLng(-90, -180), new LatLng(90, 180));
        googleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        placeAutocompleteAdapter = new PlaceAutocompleteAdapter(this, googleApiClient, LAT_LNG_BOUNDS, typeFilter);
        autoCompleteTextView1.setAdapter(placeAutocompleteAdapter);
        autoCompleteTextView2.setAdapter(placeAutocompleteAdapter);
        setActvClickListener(autoCompleteTextView1);
        setActvFocus(autoCompleteTextView1);
        setActvClickListener(autoCompleteTextView2);
        setActvFocus(autoCompleteTextView2);


    }

    private void setBusAutocompleteAdapter(AutoCompleteTextView autoCompleteTextView1, AutoCompleteTextView autoCompleteTextView2) {

        String[] cities = new String[1];
        cities = Utils.readFile(this, cities, "locations.TXT");
        ArrayAdapter<String> autocompleteAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, cities);
        autoCompleteTextView1.setAdapter(autocompleteAdapter);
        autoCompleteTextView2.setAdapter(autocompleteAdapter);
        setActvClickListener(autoCompleteTextView1);
        setActvFocus(autoCompleteTextView1);
        setActvClickListener(autoCompleteTextView2);
        setActvFocus(autoCompleteTextView2);
    }


    public void setTrainAutocompleteAdapter(AutoCompleteTextView autoCompleteTextView1, AutoCompleteTextView autoCompleteTextView2) {
        String[] train_stations = new String[1];
        train_stations = Utils.readFile(this, train_stations, "train_stations.TXT");
        ArrayAdapter<String> autocompleteAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, train_stations);
        autoCompleteTextView1.setAdapter(autocompleteAdapter);
        autoCompleteTextView2.setAdapter(autocompleteAdapter);
        setActvClickListener(autoCompleteTextView1);
        setActvFocus(autoCompleteTextView1);
        setActvClickListener(autoCompleteTextView2);
        setActvFocus(autoCompleteTextView2);
    }

    public void setActvFocus(AutoCompleteTextView autoCompleteTextView)
    {
        autoCompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    collapseKeyboard(v);
                }
            }
        });

    }

    public void setActvClickListener(AutoCompleteTextView autoCompleteTextView)
    {
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                collapseKeyboard(view);
            }
        });
    }
}
