package com.example.lenovo.travelapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText input_email, input_pass, input_confirm, input_firstName, input_lastName;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //View
        Button btnRegister = findViewById(R.id.btn_register);
        TextView btnLogMeIN = findViewById(R.id.btn_log_me_in);
        input_email = findViewById(R.id.register_email);
        input_pass = findViewById(R.id.register_password);
        input_confirm = findViewById(R.id.register_confirm_password);
        input_firstName = findViewById(R.id.register_firstName);
        input_lastName = findViewById(R.id.register_lastName);

        btnRegister.setOnClickListener(this);
        btnLogMeIN.setOnClickListener(this);

        auth = FirebaseAuth.getInstance();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_log_me_in) {
            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            finish();
        } else if (view.getId() == R.id.btn_register) {
            registerUser(input_email.getText().toString(), input_pass.getText().toString());
        }
    }

    private void registerUser(String email, String password) {

        if (registerCheck()) {
            auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                Utils.toastMessage(getApplicationContext(), "Error" + task.getException());
                            } else {
                                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                Utils.toastMessage(getApplicationContext(), "Registration successful");
                            }
                        }
                    });
        }

    }

    public boolean registerCheck()
    {
        if (input_firstName.getText().toString().equals("")) {
            Log.d("maasa", input_firstName.getText().toString());
            input_firstName.setError("First name field is empty");
            input_firstName.requestFocus();
            return false;
        }
        if (input_lastName.getText().toString().equals("")) {
            input_lastName.setError("Last name field is empty");
            input_lastName.requestFocus();
            return false;
        }
        if (input_email.getText().toString().equals("")) {
            input_email.setError("E-mail field is empty");
            input_email.requestFocus();
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(input_email.getText()).matches()) {
            input_email.setError("Not a valid e-mail addres");
            input_email.requestFocus();
            return false;
        }

        if (input_pass.getText().toString().equals("")) {
            input_pass.setError("Password field is empty");
            input_pass.requestFocus();
            return false;
        }
        if (input_pass.getText().length() < 6) {
            input_pass.setError("Password must be at least 6 characters long");
            input_pass.requestFocus();
            return false;
        }

        if (input_confirm.getText().toString().equals("")) {
            input_confirm.setError("Confirm password field is empty");
            input_confirm.requestFocus();
            return false;
        }

        if (!input_pass.getText().toString().equals(input_confirm.getText().toString()))
        {
            input_confirm.setError("Password not matching with confirm password");
            input_confirm.requestFocus();
            return false;
        }

        return  true;

    }
}
