package com.example.lenovo.travelapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth auth;
    private EditText input_new_password;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private FirebaseFirestore db=FirebaseFirestore.getInstance();
    private CollectionReference collectionReference= db.collection("users").document("Dan Bogdan").collection("Saved Routes");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        auth = FirebaseAuth.getInstance();
        input_new_password = findViewById(R.id.profile_new_password);

        Button btnChangePass = findViewById(R.id.profile_btn_change_pass);
        btnChangePass.setOnClickListener(this);
        setMenu();
        loadNotes();
    }

    @Override
    protected void onStart() {
        super.onStart();
//        collectionReference.addSnapshotListener(this, new EventListener<QuerySnapshot>() {
//            @Override
//            public void onEvent(QuerySnapshot queryDocumentSnapshots, FirebaseFirestoreException e) {
//                if (e != null) {
//                    return;
//                }
//
//                String data = "";
//
//                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
//                    Train train = documentSnapshot.toObject(Train.class);
//
//                    String documentId = train.getItem_id();
//                    String title = train.getItem_title();
//                    String departure = train.getDeparture();
//
//                    Log.d("moama", documentId);
//                    Log.d("moama", title);
//                    Log.d("moama", departure);
//                }
//
//
//            }
//        });
    }


    public void loadNotes() {

        collectionReference.get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        String data = "";
                        ArrayList<ListItem> trains = new ArrayList<>();
                        for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                            Train train = documentSnapshot.toObject(Train.class);
                            train.setItem_id(documentSnapshot.getId());
                            String documentId = train.getItem_id();
                            String title = train.getItem_title();
                            String departure_time = train.getDeparture_time();
                            String departure = train.getDeparture();
                            String arrival_time = train.getArrival_time();
                            String arrival = train.getArrival();
                            String price = train.getPrice();
                            String travel_time=train.getTravel_time();

                            Log.d("moama", documentId);
                            train=new Train();

                            train.setItem_title(documentId);
                            train.setDeparture(departure);
                            train.setArrival(arrival);
                            train.setDeparture_time(departure_time);
                            train.setArrival_time(arrival_time);
                            train.setPrice(price);
                            train.setTravel_time(travel_time);
                            Log.d("moama", title);
                            Log.d("moama", departure);
                            Log.d("moama", departure_time);
                            Log.d("moama", arrival_time);
                            Log.d("moama", arrival);
                            Log.d("moama", price);
                            Log.d("moama", travel_time);
                            addTrain(train, trains);
                        }
                        buildRecyclerView(trains);
                    }
                });


    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.profile_btn_change_pass)
            changePassword(input_new_password.getText().toString());

    }


    private void changePassword(String newPassword) {
        FirebaseUser user = auth.getCurrentUser();
        user.updatePassword(newPassword).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Utils.toastMessage(getApplicationContext(), "Password Changed");
                }
            }
        });
    }



    public void setMenu() {
        NavigationView navigation = findViewById(R.id.list_navigationView);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.app_search:
                        goHome();
                        return true;

                    case R.id.app_logout:
                        logoutUser();
                        return true;

                    case R.id.app_user_profile:
                        goProfile();
                        return true;

                    case R.id.app_map:

                        goMaps();
                        return true;
                }
                return false;
            }
        });
    }




    private void goHome() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
        finish();
    }

    private void goMaps() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
        finish();
    }

    private void goProfile() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
        finish();
    }

    private void logoutUser() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signOut();
        if (auth.getCurrentUser() == null) {
            startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
            finish();
        }
    }

    public void addTrain(Train train, ArrayList<ListItem> trains) {
        trains.add(new ListItem(train.getItem_id(), train.getItem_title(), train.getArrival(), train.getDeparture(), train.getArrival_time(), train.getDeparture_time(), "Preț: " + train.getPrice(), "Timpul călătoriei: " + train.getTravel_time(), train.getLink_type(), R.drawable.delete));
    }

    public void buildRecyclerView(final ArrayList<ListItem> trains) {
        recyclerView = findViewById(R.id.train_recyclerView);
        layoutManager = new LinearLayoutManager(this);
        TrainRecyclerViewAdapter trainRecyclerViewAdapter = new TrainRecyclerViewAdapter(trains);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(trainRecyclerViewAdapter);

        trainRecyclerViewAdapter.setOnItemClickListener(new TrainRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onMapClick(int position) {
                trains.get(position).seeMap(trains.get(position).getDeparture(), trains.get(position).getArrival(), getApplicationContext());
            }
        });
    }
}
